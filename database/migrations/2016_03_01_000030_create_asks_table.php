<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAsksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Create table for storing roles
        Schema::create('asks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('width')->nullable();
            $table->integer('height')->nullable();
            $table->integer('toppx')->nullable();
            $table->integer('leftpx')->nullable();
            $table->string('backgroundcolor')->nullable();
            $table->string('body');
            $table->integer('author_id')->unsigned();
            $table->integer('test_id')->unsigned();
            $table->integer('question_id')->unsigned();
            $table->integer('points')->nullable();
            $table->text('css')->nullable();
            $table->string('type');
            $table->timestamps();

            $table->foreign('author_id')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('test_id')->references('id')->on('tests')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('question_id')->references('id')->on('questions')
                ->onUpdate('cascade')->onDelete('cascade');
            //$table->foreign('author_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('asks');
    }
}
