<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Category;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     *
     * @return void
     */
    public function run()
    {
        $category = Category::firstOrNew([
            'slug' => 'iq',
        ]);
        if (!$category->exists) {
            $category->fill([
                'name'       => 'IQ',
            ])->save();
        }

        $category = Category::firstOrNew([
            'slug' => 'geography',
        ]);
        if (!$category->exists) {
            $category->fill([
                'name'       => 'Geography',
            ])->save();
        }
    }
}
