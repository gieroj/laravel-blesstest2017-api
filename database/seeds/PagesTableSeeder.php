<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Page;

class PagesTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     *
     * @return void
     */
    public function run()
    {
        $page = Page::firstOrNew([
            'slug' => 'index',
        ]);
        if (!$page->exists) {
            $page->fill([
                'author_id' => 0,
                'title'     => 'Bless Test Quiz',
                'excerpt'   => 'This page will contain Test Quests Courses check your knowledge from love chemistry iq math biology scientist geography language',
                'body'      => '<p>This page will contain Test Quests check your knowledge from love chemistry iq math biology scientist geography language</p>
                                <p>You can creat edit and take a courses here form every kind of knowlage.</p>
                                <p>Check your self or other people</p>',
                'image'            => 'pages/AAgCCnqHfLlRub9syUdw.jpg',
                'meta_description' => 'This page will contain Test Quests Courses check your knowledge from love chemistry iq math biology scientist geography language',
                'meta_keywords'    => 'bless test, questions, curses',
                'status'           => 'ACTIVE',
            ])->save();
        }
    }
}
