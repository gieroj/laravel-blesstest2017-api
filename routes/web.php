<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::get('/', 'PagesController@show')->name('index');
Route::get('posts/', 'PagesController@showAll')->name('pages');

Route::get('posts/', 'PostsController@showAll')->name('posts');
Route::get('post/{post_slug}/', 'PostsController@show')->name('post');

Route::get('tests/', 'PostsController@showAll')->name('tests');

Route::get('/api/tests/', 'TestsApiController@index');
Route::get('/api/tests/{id?}', 'TestsApiController@show');

Route::any('/admin/editor/{any?}', function ($any = null) {
    return view('angular',['any' => $any ]);
});

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
