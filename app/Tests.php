<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use TCG\Voyager\Facades\Voyager;
use App\Questions;
class Tests extends Model
{
//    public function authorId(){
//        return $this->belongsTo('App\User');
//    }
    
    public function save(array $options = [])
    {
        // If no author has been assigned, assign the current user's id as the author of the post
        if (!$this->author_id && Auth::user()) {
            $this->author_id = Auth::user()->id;
        }

        parent::save();
    }

    public function authorId()
    {
        return $this->belongsTo(User::class);
    }
    
    public function questions()
    {
        $question_tab = Questions::where('test_id',$this->id)->get();
        
        if($question_tab){
            return $question_tab;
        }else{
            return null;
        }
    }
}
