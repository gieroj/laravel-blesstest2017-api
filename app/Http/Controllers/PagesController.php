<?php

namespace App\Http\Controllers;

use App\Pages;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PagesController extends Controller
{
    public function show()
    {
        $page = Pages::where('slug','index')->first();
        return view('welcome',['page' => $page ]);
    }
    public function showAll()
    {
        $pages = Pages::all();
        return view('welcome',['pages' => $pages ]);
    }
}
