<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=1.0" />
        <meta name="description" content="{{$page->meta_description}}"/>
        <meta name="keywords" content="{{$page->meta_keywords}}"/>
        <meta property="og:title" content="Bless Test"/>
        <meta property="og:description" content="{{$page->meta_description}}" />
        <meta property="og:site_name" content="BlessTest.com"/>
        <meta property="og:url" content="http://blesstest.com"/>
        <meta name="twitter:title" content="Bless Test" />
        <meta name="twitter:description" content="{{$page->meta_description}}" />
        <title>Bless Test</title>
        <link rel="icon" type="image/x-icon" href="favicon.ico" />
        <link rel="stylesheet" type="text/css" href="css/app.css">
        <link rel="stylesheet" type="text/css" href="css/style.css">

        
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-41619978-1', 'auto');
            ga('send', 'pageview');

        </script>
    </head>
    <body>
        <div class="container-fluid">
<!--        <img id='backbround_photo' src="{{url('/')}}/storage/{{$page->image}}" alt="Page image"/>-->
        <div class="main_menu">
        @php echo(menu('main', 'bootstrap')); @endphp
        </div>
        <div class="flex-center position-ref full-height">
             
            @if (Auth::check())
                <div class="navbar-text navbar-right">
                        <a href="{{ url('/admin') }}">Voyager</a>
                </div>
            @endif

            <div class="content">
                <h1 class="title">{{$page->title}}</h1>
                <h2>{{$page->excerpt}}</h2>
        {!! $page->body !!}
            {{$page->slug}}


                <?php
//                dd(Auth::check());
// echo(menu('admin', 'bootstrap')); ?>
            </div>
        </div>
        </div>
        
        <!-- Load Facebook SDK for JavaScript -->
  <div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.9&appId=806760809385089";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

  <!-- Your like button code -->
  <div class="fb-like" data-href="https://blesstest.com" data-layout="button" data-action="like" data-size="small" data-show-faces="false" data-share="false"></div>
    </body>
</html>
