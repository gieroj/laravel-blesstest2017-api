
@extends('voyager::master')
    @section('head')
    <title>Angular QuickStart</title>
    <base href="http://192.168.1.100/blesstest/public/angular/src/">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="styles.css">

    <!-- Polyfill(s) for older browsers -->
    <script src="../node_modules/core-js/client/shim.min.js"></script>

    <script src="../node_modules/zone.js/dist/zone.js"></script>
    <script src="../node_modules/systemjs/dist/system.src.js"></script>

    <script src="systemjs.config.js"></script>
    <link rel="icon" type="image/x-icon" href="/favicon.ico" />
        <link rel="stylesheet" type="text/css" href="../../css/app.css">
        <link rel="stylesheet" type="text/css" href="../../css/style.css">
    <script>
      System.import('main.js').catch(function(err){ console.error(err); });
    </script>
    ');
    @stop
 @section('content')
    <my-app>Loading AppComponent content here ...</my-app>
@stop
