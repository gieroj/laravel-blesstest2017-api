@extends('index.index')

@section('content')

<button type="button" class="btn btn-primary " onclick="window.history.back();"><span class="glyphicon glyphicon-arrow-left"></span> Go Back</button>
<!--@if(Auth::check() && Auth::user()->can('test_add'))-->
<!--<a href="{{$_SERVER['REQUEST_URI'].'/create'}}" target="_self"> 
    <button type="button" class="btn btn-success "><span class="glyphicon glyphicon-plus"></span> Add New</button>
</a>
@endif-->

<div class="panel panel-default dashboard_media">
    <div class="panel-heading text-center">News</div>
    <div class="panel-body">
        @foreach($posts as $post)
            <div class="col-xs-3 ">
                <a href="{{url('/post/'.$post->slug)}}">
                    <img src="{!! url('/storage/'.$post->image) !!}" alt="{!! $post->excerpt !!} Test Picture" class="img-thumbnail"/>
                    <p> {{$post->excerpt}}</p>
                </a>
            </div>
        @endforeach
    </div>
</div>


@endsection