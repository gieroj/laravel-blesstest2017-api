<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" type="image/x-icon" href="favicon.ico" />
        <title>Bless Test</title>

        <link rel="stylesheet" type="text/css" href="{{ url('/css/app.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ url('/css/style.css') }}">

        
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-41619978-1', 'auto');
            ga('send', 'pageview');

        </script>
        <meta name="csrf-token" content="{!! csrf_token() !!}">
        @yield('head')
    </head>
    <body id="dashboard">
        <nav class="navbar navbar-default">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        <img src="{{ url('/storage/'.setting('logo'))}}" alt="BlessTest" class="logo" />
                      
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        <li><a href="{{ URL::to('admin')}}">Dashboard</a></li>
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                        <li><a href="{{ url('/login') }}">Login</a></li>
                        <li><a href="{{ url('/register') }}">Register</a></li>
                        @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                <img src="{{ url('/storage/'.Auth::user()->avatar) }}" alt="avatar" class="img-circle profile-img"/>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url('admin') }}"><i class="fa fa-btn fa-sign-out"></i>Panel</a></li>
                                <li><a href="{{ url('admin/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                            </ul>
                        </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>

        <div class="container-fluid">
            <div class="row">
<!--                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6">
                    <div class="list-group">
                        <?php
//                        $menu = Menu::get('MyNavBar')->roots();
//                dd($menu[0]);
                        ?>
     
                    </div>
                </div>-->
                <div class="col-xs-12">
                <div class="flash-message">
                    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                    @if(Session::has('alert-' . $msg))

                    <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                    @endif
                    @endforeach
                </div> <!-- end .flash-message -->
                @yield('content')
                </div>
                
            </div>
        </div>
        <!-- JavaScripts -->
        <!--<script src="{{ url('/js/angular.min.js') }}"></script>-->
        
        <script src="{{ url('/js/bootstrap.min.js') }}"></script>
        
        <script src="{{ url('/js/app.js') }}"></script>
</body>
</html>
